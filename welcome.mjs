import fs from "fs";

const content = "Hello Node";

fs.writeFile("welcome.txt", content, (err) => {
  if (err) {
    console.error("Erreur lors de la création du fichier welcome.txt :", err);
  } else {
    console.log("fichier welcome.txt crée !");
  }
});

fs.readFile("welcome.txt", "utf8", (err, data) => {
  if (err) {
    console.error("Erreur lors de la lecture du fichier welcome.txt :", err);
  } else {
    console.log("Contenu de welcome.txt :\n", data);
  }
});
