import { generate } from "generate-password";

function generateRandomPassword() {
  const password = generate({
    length: 12, // Change this number to adjust the length of the generated password
    numbers: true,
    symbols: true,
    uppercase: true,
    excludeSimilarCharacters: true,
  });

  return password;
}

const randomPassword = generateRandomPassword();
console.log("Mot de passe généré:", randomPassword);
